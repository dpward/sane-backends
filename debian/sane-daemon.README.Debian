sane-daemon (sane-backends) for Debian
--------------------------------------

This package contains saned, the daemon used to share scanners over the network.


saned setup
-----------

Please read this file carefully to setup your saned server properly.

 -> Setting up permissions for saned
 -> Running saned from inetd
 -> Running saned from systemd
 -> Running saned from sysvinit as a standalone daemon


 Setting up permissions for saned
 --------------------------------

A saned user and a saned group have been added to your system; it is highly
recommended to run saned as user and group saned and NEVER EVER as root.

To operate properly, saned must be able to access your scanners. By default,
the saned user is added to the scanner group, making ALL scanners on your
system available over the network without restrictions. To change this, run:
 # dpkg-reconfigure sane-daemon

Then for each scanner you want to make available over the network, the saned
user (or saned group) needs read and write permissions to it. For USB and SCSI
scanners, the permissions are managed by udev rules, which can be overridden.
See /usr/share/doc/libsane1/README.Debian.gz for more information on setting
appropriate permissions.

 Running saned from inetd
 ------------------------

To enable saned in the default configuration (running as user and group saned),
use update-inetd to enable the service:
 # update-inetd --enable sane-port
To disable saned, use update-inetd to disable the service:
 # update-inetd --disable sane-port

If you want to run saned under another user and/or group, edit /etc/inetd.conf
and change saned:saned for the appropriate user:group combination.

 Running saned from systemd
 --------------------------

To enable saned in the default configuration (running as user and group saned),
use systemctl to enable the service:
 # systemctl enable saned.socket
To disable saned, use systemctl to disable the service:
 # systemctl disable saned.socket


 Running saned from sysvinit as a standalone daemon
 --------------------------------------------------

When run as a standalone daemon, saned can advertise its services over the
network, and the SANE clients will automatically discover the saned servers
(provided the clients support this).

To enable saned in the default configuration (running as user and group saned),
use update-rc.d to enable the service:
 # update-rc.d saned enable
To disable saned, use update-rc.d to disable the service:
 # update-rc.d saned disable


-- Julien BLACHE <jblache@debian.org>, Mon, 05 May 2008 19:29:28 +0200
-- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 16 Dec 2014 22:04:23 +0100
