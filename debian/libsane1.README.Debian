libsane (sane-backends) for Debian :
------------------------------------

GENERAL
-------

The configuration files for Debian releases of SANE are located in /etc/sane.d.

The dll pseudo-backend is responsible for loading other SANE backends that
provide support for the actual hardware. Which backends are loaded is
determined by the contents of the /etc/sane.d/dll.conf file. The dll
pseudo-backend also checks for dll.conf snippets in /etc/sane.d/dll.d; any
file in this directory that doesn't look like a backup file will be treated
as a configuration snippet.

This facility is used by packages providing external backends (like
hpoj or hplip) to "register" the backends they provide without
much hassle.

Each backend has a configuration file which specifies which devices,
access methods, options etc. should be used by this backend. The format
and content of each configuration file is documented in the manpage for
the backend, e.g. sane-plustek (5).

For USB and some SCSI scanners, the parameters can be auto-detected, and
manual configuration is not required. If the auto-detection fails, read
the next paragraph. Again, see the manpage for your backend for more
information.

For SCSI devices (mostly scanners), the configuration files use the
/dev/scanner device; /dev/scanner is a symbolic link to the appropriate
SCSI device node. It's up to you to create this symbolic link, once you
will have determined which device node it needs to point to. Use the
sane-find-scanner command in the libsane-utils package to detect which
SCSI device your scanner is attached to. The sane-find-scanner utility
also discovers USB scanners.

It can be a good idea to try running sane-find-scanner as root to ensure
there will be no permissions problems while attempting to detect your
devices.


DOCUMENTATION
-------------

For information on configuring and trouble-shooting the various SANE
components, please refer to the manual pages listed below:

       Regarding:	  Read:
       -----------------  ------------------------------------------
       General            sane(7) -- your starting point

       scanimage	  scanimage(1)
       xscanimage	  xscanimage(1)
       saned		  saned(8)
       xcam		  xcam(1)

       Dynamic loading	  sane-dll(5)
       Backends           See sane-<backend name>(5). Each backend
                          comes with a manual page in section 5 of
                          the manual system.


SETUP
-----

In this day and age, SANE integrates with udev and ConsoleKit/systemd-logind
seemlessly; this means users physically logged into the machine (as opposed
to users logged in remotely via SSH) have access to the scanners by default.

The solution proposed below is a legacy setup that remains valid for sharing
scanners with saned or for systems that don't use ConsoleKit/systemd-logind.
Note that this is only a proposed solution, you are free to come up with and
implement whatever access control mechanism you see fit.

This package added a scanner group to your system. Device files for scanners
(e.g. /dev/sg0) should be readable and writable by this group. Any users who
are added to this group will be able to access your scanners, even remotely.

For USB and SCSI scanners, the permissions will be set automatically by udev.
The /lib/udev/rules.d/60-libsane1.rules file contains default udev rules for
the scanners supported by SANE. When a device matches one of these rules, an
entry is added to the device file's ACL giving read and write permissions to
the scanner group. (This does not change the ownership of the device file.)

If a udev rule for your scanner is missing, or to make other changes, DO NOT
modify the default udev rules file; it WILL be overwritten during upgrades.
Copy it to /etc/udev/rules.d/60-libsane1.rules, and make your changes there;
udev will read this instead of the original file. To run a command when your
scanner is found, add RUN+="/path/to/command args" to the matching udev rule.
For example, RUN+="/bin/setfacl -m u:saned:rw $env{DEVNAME}" can be used to
give the saned user read and write permissions to the scanner's device file.

Feel free to file a wishlist severity bug report against the libsane1 package
to get your scanner added; please mention which backend you use and how well
the scanner is supported (basic, good, ...).

  Note: please do not file bugs requesting the addition of scanners that
  aren't supported by the libsane package. For these devices, bugs should
  be filed against the Debian package providing support for the device, if
  such a package does exist.


TROUBLESHOOTING
---------------

If your scanner does not work, edit the file /etc/sane.d/dll.conf.
Verify that your scanner is not commented out. You may need to
comment out all other scanners in dll.conf. It shouldn't matter, but
sometimes it does.

The most common cause for a non-working scanner is inappropriate
permissions on the device. So your first reflex should be to check the
permissions of the device used to access your scanner, e.g. /dev/sg0
or the device pointed to by /dev/scanner.

If running "scanimage > t.pnm" gives an error like "scanimage: open of
device niash:libusb:002:005 failed: Device busy", powercycling your
scanner might help.

If you encounter any problems with getting your device(s) recognized,
try setting the various environment variables that are there to assist
in debugging such problems. The environment variables are documented
in the relevant manual pages. For example, to get the maximum amount
of debug information when testing a Mustek scanner, set environment
variables SANE_DEBUG_DLL, SANE_DEBUG_MUSTEK, and SANE_DEBUG_SANEI_SCSI
to 128 and then invoke scanimage or whatever program you're trying to
debug.  For a Mustek SCSI scanner at /dev/scanner, you might want to
invoke scanimage as follows:

	scanimage -d mustek:/dev/scanner -h

If this works, you could try to acquire an image with:

	scanimage -d mustek:/dev/scanner > t.pnm

If you are not sure what generic SCSI device your scanner is connected
to, try the command sane-find-scanner (libsane-utils package). It is
normally sufficient to invoke the program without any arguments. Invoking
this command should produce output similar to this:

  $ sane-find-scanner
  sane-find-scanner: found "MUSTEK MFC-06000CZ 1.01" at device /dev/sge

sane-find-scanner will help you discover your USB scanner, too.

For some more help can read TROUBLESHOOTING.debian in the doc directory.


REPORTING BUGS
--------------

When reporting a bug, be it to the SANE developers or to the Debian bug
tracking system, pleases always provide:
 - the full version of libsane
 - the backend you're using
 - the configuration of the backend
 - the debug output, obtained by setting the environment variable
   SANE_DEBUG_<backendname> to a value of 255 (see above, TROUBLESHOOTING)

Without that, your bug report will take longer to be processed, because we'll
need to ask you for each of these items. Please help us help you.

-- Julien BLACHE <jblache@debian.org>, Wed, 16 Feb 2011 19:00:12 +0100
