# translation of sane-backends_1.0.21-4.1_eu.po to Basque
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Piarres Beobide <pi@beobide.net>, 2008.
# Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: sane-backends_1.0.21-4.1_eu\n"
"Report-Msgid-Bugs-To: sane-backends@packages.debian.org\n"
"POT-Creation-Date: 2009-12-12 13:14+0100\n"
"PO-Revision-Date: 2010-10-09 13:12+0200\n"
"Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>\n"
"Language-Team: Basque <debian-l10n-basque@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../sane-daemon.templates:1001
msgid "Add saned user to the scanner group?"
msgstr "Gehitu saned erabiltzailea scanner taldera?"

#. Type: boolean
#. Description
#: ../sane-daemon.templates:1001
msgid ""
"The saned server, when enabled, makes scanners available over the network. "
"By applying different permissions to the different scanners connected to "
"your machine, you can control which ones will be made available over the "
"network."
msgstr ""
"Saned zerbitzariak, gaituta dagoenean, eskanerrak sarearen bidez erabilgarri "
"egiten ditu. Zure makinara konektatuta dagoen eskaner bakoitzari baimen "
"desberdinak ezarriz horietariko zein sarearen bidez erabilgarri izango diren "
"kontrola dezakezu."

#. Type: boolean
#. Description
#: ../sane-daemon.templates:1001
msgid ""
"Read /usr/share/doc/sane-daemon/README.Debian for details on how to manage "
"permissions for saned. By default, saned is run under the saned user and "
"group."
msgstr ""
"Irakurri /usr/share/doc/sane-daemon/README.Debian saned-eko baimen "
"kudeaketari buruz xehetasun gehiagorako. Lehenetsi gisa saned zerbitzaria "
"saned erabiltzaile eta taldearekin exekutatzen da."

#. Type: boolean
#. Description
#: ../sane-daemon.templates:1001
msgid ""
"Accept this option if you want to make all your scanners available over the "
"network without restriction."
msgstr ""
"Onartu aukera hau zure eskaner guztiak sarearen bidez eta mugarik gabe "
"erabilgarri egin bahi badituzu."
